#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# set keyboard options
setxkbmap -option ctrl:nocaps

alias ls='ls --color=auto'
alias ll='ls -l'
alias r='ranger'
export PS1="\n\[\033[38;5;203m\]\u@\h \[$(tput sgr0)\]\[\033[38;5;39m\][\[$(tput sgr0)\]\[\033[38;5;2m\]\A\[$(tput sgr0)\]\[\033[38;5;39m\]]\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;82m\]\w\[$(tput sgr0)\]  \[$(tput sgr0)\]"

