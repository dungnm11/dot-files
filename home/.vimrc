"----set common options----
set nocompatible              " be iMproved, required
filetype off                  " required

set number                                                  " turn on line
set relativenumber
syntax on                                                   " high light

set encoding=utf-8

set tabstop=4
set shiftwidth=4
set expandtab
set nowrap
set hlsearch
set splitright

" scroll offset
set so=5

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" git
Plugin 'tpope/vim-fugitive'

" code navigator
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'ryanoasis/vim-devicons'

" langauge helper
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}  " html tag helper
Plugin 'scrooloose/syntastic'               " code hightlight
Plugin 'ervandew/supertab'                  " code complete
Plugin 'scrooloose/nerdcommenter'	        " commenter
Plugin 'majutsushi/tagbar'	                " tagbar show code tree
Plugin 'ntpeters/vim-better-whitespace'	    " show and remove trailing whitespaces

" code display
Plugin 'tpope/vim-eunuch'
Plugin 'rakr/vim-one'
Plugin 'itchyny/lightline.vim'
Plugin 'mcmartelle/vim-monokai-bold'

call vundle#end()            " required
filetype plugin indent on    " required

autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
autocmd FileType json setlocal shiftwidth=2 tabstop=2
autocmd FileType yaml setlocal shiftwidth=2 tabstop=2

" setting plugin
let g:strip_whitespace_on_save=1
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

" set code display
colorscheme monokai-bold
" set background=dark
" set background=light
hi Normal guibg=NONE ctermbg=NONE

" map key
imap jj <Esc>
imap jk <Esc>
imap kj <Esc>

