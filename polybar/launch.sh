#!/bin/bash

killall -q polybar

while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# base from nord theme: https://github.com/Murzchnvok/polybar-collection
polybar nord-dungnm &

echo "Polybar lauched..."
